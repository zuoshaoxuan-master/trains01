package com.zsj.testproject.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPO {
    private Integer id;
    private String userName;
    private String idCard;
    private Date brthDate;
    private List<RolePO> rolePOList=new ArrayList<>();
}
