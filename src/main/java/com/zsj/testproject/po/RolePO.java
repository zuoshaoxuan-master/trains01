package com.zsj.testproject.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RolePO {
    private Integer id;
    private String roleName;
    private String deleteFalg;
}
