package com.zsj.testproject.controller;

import com.zsj.testproject.po.*;
import com.zsj.testproject.resp.ResponseVO;
import com.zsj.testproject.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/zsj/project/role")
public class RoleController {

    @Autowired
    private RoleService roleService;


    @PostMapping("/insert")
    private ResponseVO insert(@RequestBody  RolePO rolePO){
        ResponseVO vo=new ResponseVO();
        roleService.insert(rolePO);
        return vo;
    }


}
