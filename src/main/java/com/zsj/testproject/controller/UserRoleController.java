package com.zsj.testproject.controller;

import com.zsj.testproject.po.UserPO;
import com.zsj.testproject.resp.ResponseVO;
import com.zsj.testproject.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/zsj/project/userrole")
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @PostMapping("/selectByuserId")
    public ResponseVO selectByuserId(@RequestParam String userId){
        ResponseVO vo=new ResponseVO();
        List<UserPO> userPO=userRoleService.selectByuserId(userId);
        vo.setData(userPO);
        return vo;
    }
}
