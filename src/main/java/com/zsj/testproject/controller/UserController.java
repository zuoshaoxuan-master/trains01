package com.zsj.testproject.controller;

import com.zsj.testproject.po.UserPO;
import com.zsj.testproject.req.UserReq;
import com.zsj.testproject.resp.ResponseVO;
import com.zsj.testproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/zsj/project/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert")
    public ResponseVO insert(@RequestBody UserReq userReq){
        ResponseVO vo=new ResponseVO();
        UserPO userPO=new UserPO();
        userPO.setUserName(userReq.getUserName());
        userPO.setIdCard(userReq.getIdCard());
        userPO.setBrthDate(new Date());
        int row=userService.insertUserService(userPO);
//        userPO.getId() 新增成功后返回数据主键id
        vo.setData(row);
        return vo;
    }

    @GetMapping ("/select")
    public ResponseVO select(){
        ResponseVO vo=new ResponseVO();
        UserPO userPO=userService.selectAll();
        vo.setData(userPO);
        vo.setCode("10000");
        vo.setMeg("请求成功");
        return vo;
    }


}
