package com.zsj.testproject.dao;

import com.zsj.testproject.po.UserPO;

import java.util.List;

public interface UserDAO {

     int insertUser(UserPO userPO);

     UserPO selectAll();

     List<UserPO> selectByuserId(String userId);
}
