package com.zsj.testproject.resp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseVO {
    private Object data;
    private String code="10000";
    private String meg="请求成功";

}
