package com.zsj.testproject.service.impl;

import com.zsj.testproject.dao.RoleDAO;
import com.zsj.testproject.po.RolePO;
import com.zsj.testproject.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDAO roleDAO;

    @Override
    public void insert(RolePO rolePO) {
        roleDAO.insertRole(rolePO);
    }
}
