package com.zsj.testproject.service.impl;

import com.zsj.testproject.dao.UserDAO;
import com.zsj.testproject.po.RolePO;
import com.zsj.testproject.po.UserPO;
import com.zsj.testproject.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author:左世杰
 * @Description:用户角色逻辑实现层
 * @DateTime:2021-02-19 10:36
 **/
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public List<UserPO> selectByuserId(String userId) {
        return userDAO.selectByuserId(userId);
    }
}
