package com.zsj.testproject.service.impl;

import com.zsj.testproject.dao.UserDAO;
import com.zsj.testproject.po.UserPO;
import com.zsj.testproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;


    @Override
    public int insertUserService(UserPO userPO) {
       return userDAO.insertUser(userPO);
    }

    @Override
    public UserPO selectAll() {
        return userDAO.selectAll();
    }
}
