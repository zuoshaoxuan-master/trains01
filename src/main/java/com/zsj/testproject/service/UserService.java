package com.zsj.testproject.service;

import com.zsj.testproject.po.UserPO;

public interface UserService {

    public int insertUserService(UserPO userPO);

    public UserPO selectAll();

}
