package com.zsj.testproject.service;

import com.zsj.testproject.po.RolePO;
import com.zsj.testproject.po.UserPO;

import java.util.List;

/**
 * @Author:左世杰
 * @Description:用户角色管理逻辑层
 * @DateTime:2021-02-19 10:35
 **/

public interface UserRoleService {

    List<UserPO> selectByuserId(String userId);

}
