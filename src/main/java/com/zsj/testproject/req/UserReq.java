package com.zsj.testproject.req;

import lombok.Data;

@Data
public class UserReq {

    private String userName;
    private String idCard;

}
